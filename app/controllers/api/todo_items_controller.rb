class Api::TodoItemsController < Api::ApiController
  before_filter :find_todo_list

  def index
    render json: @list.todo_items
  end

  def create
    item = @list.todo_items.new(todo_item_params)
    if item.save
      render status: 200, json: {
        message: "Successfully created todo item",
        todo_list: @list,
        todo_item: item
      }.to_json
    else
      render status: 422, json: {
        message: "Todo item creation failed",
        errors: item.errors
      }.to_json
    end
  end

  def update
    item = @list.todo_items.find(params[:id])
    if item.update(todo_item_params)
      render status: 200, json: {
        message: "Successfully updated todo item",
        todo_list: @list,
        todo_item: item
      }.to_json
    else
      render status: 422, json: {
        message: "Todo item update failed",
        errors: item.errors
      }.to_json
    end
  end

  def destroy
    item = @list.todo_items.find(params[:id])
    item.destroy
    render status: 200, json: {
      message: "Successfully deleted todo item",
      todo_list: @list,
      todo_item: item
    }.to_json
  end

  private

  def todo_item_params
    params.require(:todo_item).permit(:content)
  end

  def find_todo_list
    @list = TodoList.find(params[:todo_list_id])
  end
end
