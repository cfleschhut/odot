Odot::Application.routes.draw do

  namespace :api do
    resources :todo_lists do
      resources :todo_items, only: [:index, :create, :update, :destroy]
    end
  end

  resources :todo_lists do
    resources :todo_items do
      member do
        patch :complete
      end
    end
  end

  root 'todo_lists#index'

end
