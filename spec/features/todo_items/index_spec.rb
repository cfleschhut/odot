require 'rails_helper'

describe 'viewing todo items' do

  let!(:todo_list) do
    TodoList.create(
      title: 'Grocery list',
      description: 'Groceries'
    )
  end

  it 'displays the title of the todo list' do
    visit_todo_list(todo_list)
    within 'h2' do
      expect(page).to have_content(todo_list.title)
    end
  end

  it 'displays no items when a todo list is empty' do
    visit_todo_list(todo_list)
    expect(page.all('.todo_items li').size).to eq(0)
  end

  it 'displays item content when a todo list has items' do
    todo_list.todo_items.create(content: 'Milk')
    todo_list.todo_items.create(content: 'Eggs')

    visit_todo_list(todo_list)

    expect(page.all('.todo_items li').size).to eq(2)

    within '.todo_items' do
      expect(page).to have_content('Milk')
      expect(page).to have_content('Eggs')
    end
  end

end
