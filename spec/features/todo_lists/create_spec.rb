require 'rails_helper'

describe 'creating todo lists' do

  it 'redirects to the todo lists index page on success' do
    visit todo_lists_path
    click_link 'New Todo list'
    expect(page).to have_content('New todo list')

    fill_in 'Title', with: 'My todo list'
    fill_in 'Description', with: 'This is what i’m doing today.'
    click_button 'Create Todo list'

    expect(page).to have_content('My todo list')
    # expect(current_path).to eq(todo_lists_path)
  end

end
